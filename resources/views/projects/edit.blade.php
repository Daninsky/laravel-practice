@extends('layout')
@section('content')

<h1>Edit project</h1>

<form method="POST" action="/projects/{{ $project->id }}">
    {{ method_field('PUT') }}
    {{ csrf_field() }}

    <div class="field">
        <label class="label" for="title">Title</label>
        <div class="control">
            <input type="text" class="input" name="title" placeholder="Title" value="{{ $project->title }}">
        </div>
    </div>

    <div class="field">
        <label class="label" for="title">Description</label>
        <div class="control">
            <textarea type="text" class="textarea" name="description">{{ $project->description }}</textarea>
        </div>
    </div>

    <div class="field">
        <div class="control">
           <button type="submit" class="button">Update project</button>
        </div>
    </div>
</form>

<form method="POST" action="/projects/{{ $project->id }}">
    {{ method_field('DELETE') }}
    {{ csrf_field() }}
    <div class="field">
        <div class="control">
           <button type="delete" class="button">Delete project</button>
        </div>
    </div>

</form>


@endsection