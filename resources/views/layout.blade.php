<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title','Default')</title>
    <!--link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css"-->

    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            display: table;
            font-weight: 100;
            font-family: 'Lato';
        }

        .container {
            text-align: center;
            display: block;
            vertical-align: middle;
            position: relative;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 96px;
            display: inline-block;
        }
    </style>

</head>
<body>
    @yield('content')
    
    <div class="container" style="clear:both;">
        <div class="content">
        <ul>
            <li><a href="/">Home</a></li>
            <li><a href="/about">About Me</a></li>
            <li><a href="/contact">Contact Me</a></li>
        </ul>
        </div>
    </div>
</body>
</html>