<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    public function home() {
        return view('welcome');
    }    

    public function about() {
        $about = [
            'My name is Danin',
            'I am mahasiswa at University Indonesia',
            'I want a Nintendo Switch'
        ];
    
        return view('about')->withAbout($about);
    }

    public function contact() {
        return view('contacts');
    }
}
