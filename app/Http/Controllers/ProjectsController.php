<?php

namespace App\Http\Controllers;

use App\project;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use ValidationRequests;

class ProjectsController extends Controller
{
    public function index() {
        $projects = project::all();
        return view('projects.index', compact('projects'));
    }

    public function create() {
        return view('projects.create');
    }

    public function store(Request $request) {
        $this->validate($request,[
            'title' => ['required', 'min:3'],
            'description' => ['required', 'min:3']
        ]);
        project::create(request()->only(['title','description']));      
        return redirect('/projects');
    }

    public function show(project $project) {
        return view('projects.show', compact('project'));
    }

    public function edit(project $project) {
        return view('projects.edit', compact('project'));
    }

    public function update(project $project) {
        $project->update(request()->only(['title','description']));

        return redirect('/projects');
    }

    public function destroy(project $project) {
        $project->delete();

        return redirect('/projects');

    }

}
